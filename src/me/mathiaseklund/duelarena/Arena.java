package me.mathiaseklund.duelarena;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;

import net.md_5.bungee.api.ChatColor;

public class Arena {

	Main main;
	Util util;
	File f;
	FileConfiguration fc;
	String name;
	String displayName;
	List<String> displayLore;
	String displayMaterial;
	int displayAmount;
	boolean displayGlow;

	Location spawn1;
	Location spawn2;

	List<ItemStack> kit;

	/**
	 * Load existing arena.
	 * 
	 * @param file
	 */
	public Arena(File file) {
		main = Main.getMain();
		util = main.getUtil();
		f = file;
		name = f.getName().replace(".yml", "");
		load();
	}

	/**
	 * Create new arena.
	 * 
	 * @param name
	 */
	public Arena(String name) {
		main = Main.getMain();
		util = main.getUtil();
		this.name = name;
		create();
	}

	/**
	 * Create new data
	 */
	void create() {
		f = new File(main.getDataFolder() + "/arenas/" + name + ".yml");
		if (!f.exists()) {
			try {
				f.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		fc = YamlConfiguration.loadConfiguration(f);

		fc.set("display.name", "&7" + name);
		fc.set("display.lore", new ArrayList<String>().add("&eClick to select this arena."));
		fc.set("display.material", "STONE");
		fc.set("display.glow", false);
		fc.set("display.amount", 1);
		save();

		load();

	}

	/**
	 * Load existing data
	 */
	void load() {
		fc = YamlConfiguration.loadConfiguration(f);

		// load spawns
		String location = fc.getString("spawn.1");
		if (location != null) {
			String[] parts = location.split("@");
			Location loc = new Location(Bukkit.getWorld(parts[0]), Integer.parseInt(parts[1]),
					Integer.parseInt(parts[2]), Integer.parseInt(parts[3]));
			if (loc != null) {
				spawn1 = loc;
			}
		}

		location = fc.getString("spawn.2");
		if (location != null) {
			String[] parts = location.split("@");
			Location loc = new Location(Bukkit.getWorld(parts[0]), Integer.parseInt(parts[1]),
					Integer.parseInt(parts[2]), Integer.parseInt(parts[3]));
			if (loc != null) {
				spawn2 = loc;
			}
		}

		displayName = fc.getString("display.name");
		displayLore = fc.getStringList("display.lore");
		displayMaterial = fc.getString("display.material");
		displayGlow = fc.getBoolean("display.glow");
		displayAmount = fc.getInt("display.amount");

		kit = new ArrayList<ItemStack>();

		ConfigurationSection section = fc.getConfigurationSection("kit");
		if (section != null) {
			for (String key : section.getKeys(false)) {
				ItemStack item = section.getItemStack(key);
				if (item != null) {
					kit.add(item);
				}
			}
		}
	}

	void save() {
		try {
			fc.save(f);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String getName() {
		return name;
	}

	public String getDisplayName() {
		return displayName;
	}

	public List<String> getDisplayLore() {
		return displayLore;
	}

	public boolean hasKit() {
		return kit != null || !kit.isEmpty();
	}

	public List<ItemStack> getKit() {
		return kit;
	}

	public boolean getDisplayGlow() {
		return displayGlow;
	}

	public String getDisplayMaterial() {
		return displayMaterial;
	}

	public Location getSpawnLocation(int i) {
		if (i == 0) {
			return spawn1;
		} else {
			return spawn2;
		}
	}

	public boolean hasSetSpawns() {
		return spawn1 != null && spawn2 != null;
	}

	public void setSpawn(int i, Location loc) {
		String location = loc.getWorld().getName() + "@" + loc.getBlockX() + "@" + loc.getBlockY() + "@"
				+ loc.getBlockZ();
		if (i == 0) {
			spawn1 = loc;
			fc.set("spawn.1", location);
			save();
		} else {
			spawn2 = loc;
			fc.set("spawn.2", location);
			save();
		}
	}

	public void setKit(List<ItemStack> list) {
		kit = list;
		for (int i = 0; i < list.size(); i++) {
			fc.set("kit." + i, list.get(i));
		}
		save();
	}

	public void setDisplayName(String s) {
		this.displayName = s;
		fc.set("display.name", s);
		save();
	}

	public void setDisplayLore(List<String> list) {
		this.displayLore = list;
		fc.set("display.lore", list);
		save();
	}

	public void setDisplayMaterial(String s) {
		this.displayMaterial = s;
		fc.set("display.material", s);
		save();
	}

	public void setDisplayGlow(boolean b) {
		this.displayGlow = b;
		fc.set("display.glow", b);
		save();
	}

	public void setDisplayAmount(int i) {
		displayAmount = i;
		fc.set("display.amount", i);
		save();
	}

	public ItemStack getDisplayItem() {
		ItemStack is = null;
		is = new ItemStack(Material.getMaterial(displayMaterial), displayAmount);
		ItemMeta im = is.getItemMeta();
		if (displayName != null) {
			im.setDisplayName(ChatColor.translateAlternateColorCodes('&', displayName));
		}

		if (displayLore != null) {
			List<String> lore = new ArrayList<String>();
			for (String s : displayLore) {
				lore.add(ChatColor.translateAlternateColorCodes('&', s));
			}
			im.setLore(lore);
		}
		im.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		if (displayGlow) {
			im.addItemFlags(ItemFlag.HIDE_ENCHANTS);

		}

		PersistentDataContainer container = im.getPersistentDataContainer();
		container.set(main.getArenas().getDisplayKey(), PersistentDataType.STRING, name);

		is.setItemMeta(im);

		if (displayGlow) {
			is.addUnsafeEnchantment(Enchantment.VANISHING_CURSE, 1);
		}
		return is;
	}

	public void giveKit(Player player) {
		for (ItemStack is : kit) {
			if (is.getType().toString().contains("_HELMET")) {
				if (player.getInventory().getHelmet() == null) {
					player.getInventory().setHelmet(is);
				} else {
					player.getInventory().addItem(is);
				}
			} else if (is.getType().toString().contains("_CHESTPLATE")) {
				if (player.getInventory().getChestplate() == null) {
					player.getInventory().setChestplate(is);
				} else {
					player.getInventory().addItem(is);
				}
			} else if (is.getType().toString().contains("_LEGGINGS")) {
				if (player.getInventory().getLeggings() == null) {
					player.getInventory().setLeggings(is);
				} else {
					player.getInventory().addItem(is);
				}
			} else if (is.getType().toString().contains("_BOOTS")) {
				if (player.getInventory().getBoots() == null) {
					player.getInventory().setBoots(is);
				} else {
					player.getInventory().addItem(is);
				}
			} else {
				player.getInventory().addItem(is);
			}
		}
	}

	public File getFile() {
		return f;
	}
}
