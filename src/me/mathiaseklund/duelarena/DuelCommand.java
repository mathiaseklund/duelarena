package me.mathiaseklund.duelarena;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class DuelCommand implements CommandExecutor {

	Main main = Main.getMain();
	Util util = main.getUtil();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			if (args.length == 0) {
				main.getDuels().startDuel(player);
			} else {
				if (args.length == 2) {
					if (args[0].equalsIgnoreCase("accept")) {
						Player target = Bukkit.getPlayer(args[1]);
						if (target != null) {
							main.getDuels().acceptedInvite(player, target);
						} else {
							util.error(sender, "Unable to find target player.");
						}
					} else if (args[0].equalsIgnoreCase("decline")) {

					}
				} else {
					util.message(sender, "&e/duel accept <player>");
					util.message(sender, "&e/duel decline <player>");
				}
			}
		}
		return false;
	}

}
