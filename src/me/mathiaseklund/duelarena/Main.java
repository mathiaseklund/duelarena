package me.mathiaseklund.duelarena;

import java.io.File;

import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import net.milkbowl.vault.economy.Economy;

public class Main extends JavaPlugin {

	static Main main;

	public static Main getMain() {
		return main;
	}

	Util util;
	Config config;
	Economy econ = null;
	Arenas arenas;
	Duels duels;

	public void onEnable() {
		main = this;

		loadFiles();
		loadDependencies();
		loadObjects();
		loadListeners();
		loadCommands();
	}

	public void onDisable() {
		main.getDuels().cancelAllDuels(true);
	}

	void loadFiles() {
		File f = new File(getDataFolder(), "config.yml");
		if (!f.exists()) {
			saveResource("config.yml", true);
		}

		f = new File(getDataFolder() + "/arenas/");
		if (!f.exists()) {
			f.mkdir();
		}
	}

	void loadDependencies() {
		// Vault
		if (!setupEconomy()) {
			System.out.println("Disabled DuelArena because Vault couldn't be found.");
			getServer().getPluginManager().disablePlugin(this);
			return;
		}
	}

	void loadObjects() {
		config = new Config(new File(getDataFolder(), "config.yml"));
		util = new Util();
		arenas = new Arenas();
		duels = new Duels();
	}

	void loadListeners() {
		getServer().getPluginManager().registerEvents(new Events(), this);
	}

	void loadCommands() {
		getCommand("duel").setExecutor(new DuelCommand());
		getCommand("arena").setExecutor(new ArenaCommand());
	}

	public Config getCfg() {
		return config;
	}

	public Util getUtil() {
		return util;
	}

	private boolean setupEconomy() {
		if (getServer().getPluginManager().getPlugin("Vault") == null) {
			return false;
		}
		RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
		if (rsp == null) {
			return false;
		}
		econ = rsp.getProvider();
		return econ != null;
	}

	public Economy getEconomy() {
		return econ;
	}

	public Arenas getArenas() {
		return arenas;
	}

	public Duels getDuels() {
		return duels;
	}
}
