package me.mathiaseklund.duelarena;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;

public class Duel {

	Main main;
	Util util;
	Player sender;
	Player target;
	double bet;
	String arenaName;
	Arena arena;

	Location senderLoc;
	Location targetLoc;

	boolean started;
	boolean cancelled = false;

	public Duel(Player sender, Player target, double bet, String arenaName) {
		main = Main.getMain();
		util = main.getUtil();
		this.sender = sender;
		this.target = target;
		this.bet = bet;
		this.arenaName = arenaName;
		arena = main.getArenas().getArena(arenaName);
		started = false;

		startDuel(main.getCfg().getDuelStartTime());
	}

	void startDuel(int time) {
		if (time == main.getCfg().getDuelStartTime()) {
			senderLoc = sender.getLocation();
			targetLoc = target.getLocation();

			try {
				sender.teleport(arena.getSpawnLocation(0));
				target.teleport(arena.getSpawnLocation(1));
			} catch (Exception e) {
				util.error(sender, "Unable to teleport to spawn location.");
				util.error(target, "Unable to teleport to spawn location.");
			}

			if (arena.getKit() != null) {
				arena.giveKit(sender);
				arena.giveKit(target);
			}
		}
		if (time == 0) {
			sender.spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(ChatColor
					.translateAlternateColorCodes('&', "&aYour duel with " + target.getName() + " has now started!")));
			target.spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(ChatColor
					.translateAlternateColorCodes('&', "&aYour duel with " + sender.getName() + " has now started!")));
			started = true;
		} else {
			sender.spigot().sendMessage(ChatMessageType.ACTION_BAR,
					new TextComponent(ChatColor.translateAlternateColorCodes('&',
							"&aYour duel with " + target.getName() + " will start in " + time + " seconds.")));
			target.spigot().sendMessage(ChatMessageType.ACTION_BAR,
					new TextComponent(ChatColor.translateAlternateColorCodes('&',
							"&aYour duel with " + sender.getName() + " will start in " + time + " seconds.")));
			Bukkit.getScheduler().runTaskLaterAsynchronously(main, new Runnable() {
				public void run() {
					startDuel(time - 1);
				}
			}, 20);
		}
	}

	public boolean isStarted() {
		return started;
	}

	public void setStarted(boolean b) {
		started = b;
	}

	public void cancelDuel(boolean disabled) {

		cancelled = true;
		if (!disabled) {
			main.getDuels().removeInDuel(sender);
			main.getDuels().removeInDuel(target);
		}

		sender.teleport(senderLoc);
		target.teleport(targetLoc);
		sender.getInventory().clear();
		target.getInventory().clear();
		sender.getInventory().setArmorContents(null);
		target.getInventory().setArmorContents(null);

		sender.setHealth(sender.getAttribute(Attribute.GENERIC_MAX_HEALTH).getBaseValue());
		target.setHealth(target.getAttribute(Attribute.GENERIC_MAX_HEALTH).getBaseValue());
		target.setFoodLevel(20);
		sender.setFoodLevel(20);

		util.message(sender, "&cYour duel with " + target.getName() + " was cancelled. Bet refunded.");
		util.message(target, "&cYour duel with " + sender.getName() + " was cancelled. Bet refunded.");

	}

	public Player getTarget() {
		return target;
	}

	public Player getSender() {
		return sender;
	}

	public double getBet() {
		return bet;
	}

	public boolean isCancelled() {
		return cancelled;
	}

	public void setCancelled(boolean b) {
		cancelled = b;
	}

	public void setWinner(Player player) {
		main.getEconomy().withdrawPlayer(target, bet);
		main.getEconomy().withdrawPlayer(sender, bet);
		main.getEconomy().depositPlayer(player, bet * 2);

		main.getDuels().removeInDuel(sender);
		main.getDuels().removeInDuel(target);

		sender.teleport(senderLoc);
		target.teleport(targetLoc);
		sender.getInventory().clear();
		target.getInventory().clear();
		sender.getInventory().setArmorContents(null);
		target.getInventory().setArmorContents(null);

		sender.setHealth(sender.getAttribute(Attribute.GENERIC_MAX_HEALTH).getBaseValue());
		target.setHealth(target.getAttribute(Attribute.GENERIC_MAX_HEALTH).getBaseValue());
		target.setFoodLevel(20);
		sender.setFoodLevel(20);

		List<String> won = main.getCfg().getDuelWon();
		List<String> lost = main.getCfg().getDuelLost();
		if (player.getName().equalsIgnoreCase(target.getName())) {
			for (String s : won) {
				util.message(target, s.replace("%player%", sender.getName()).replace("%won%", (bet * 2) + ""));
			}
			for (String s : lost) {
				util.message(sender, s.replace("%player%", target.getName()).replace("%lost%", (bet) + ""));
			}
		} else {

			for (String s : won) {
				util.message(sender, s.replace("%player%", target.getName()).replace("%won%", (bet * 2) + ""));
			}
			for (String s : lost) {
				util.message(target, s.replace("%player%", sender.getName()).replace("%lost%", (bet) + ""));
			}
		}
	}

	public void killed(Player player) {
		if (target.getName().equalsIgnoreCase(player.getName())) {
			setWinner(sender);
		} else {
			setWinner(target);
		}
	}
}
