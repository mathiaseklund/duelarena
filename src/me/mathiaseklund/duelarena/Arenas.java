package me.mathiaseklund.duelarena;

import java.io.File;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.NamespacedKey;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class Arenas {

	Main main;
	Util util;

	HashMap<String, Arena> arenas;

	NamespacedKey displayKey;

	public Arenas() {
		main = Main.getMain();
		util = main.getUtil();
		load();
	}

	void load() {

		arenas = new HashMap<String, Arena>();
		displayKey = new NamespacedKey(main, "arena");
		File folder = new File(main.getDataFolder() + "/arenas/");
		if (folder.exists()) {
			for (File file : folder.listFiles()) {
				Arena arena = new Arena(file);
				arenas.put(file.getName().replace(".yml", ""), arena);
			}
		} else {
			util.error("Arena folder not found. Unable to load arenas.");
		}
	}

	public Arena getArena(String name) {
		return arenas.get(name);
	}

	public boolean doesArenaExist(String name) {
		return arenas.containsKey(name);
	}

	public HashMap<String, Arena> getAllArenas() {
		return arenas;
	}

	public void createNewArena(CommandSender sender, String name) {
		if (!doesArenaExist(name)) {
			Arena arena = new Arena(name);
			arenas.put(name, arena);
			util.message(sender, "&eYou've created a new arena with named: " + name);
		} else {
			util.error(sender, "An arena already exists with that name.");
		}
	}

	public void deleteArena(CommandSender sender, String name) {
		if (doesArenaExist(name)) {
			Arena arena = new Arena(name);
			arenas.remove(name);
			arena.getFile().delete();
		} else {
			util.error(sender, "An arena does not exist with that name.");
		}
	}

	public Inventory getArenaSelectionGUI(Player player) {
		Inventory inv = Bukkit.createInventory(player, main.getCfg().getArenaSelectSize(),
				ChatColor.translateAlternateColorCodes('&', main.getCfg().getArenaSelectTitle()));
		for (Arena arena : getAllArenas().values()) {
			ItemStack is = arena.getDisplayItem();
			inv.addItem(is);
		}
		return inv;
	}

	public NamespacedKey getDisplayKey() {
		return displayKey;
	}
}
