package me.mathiaseklund.duelarena;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class ArenaCommand implements CommandExecutor {

	Main main = Main.getMain();
	Util util = main.getUtil();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {

		if (sender.isOp()) {
			if (args.length == 0) {
				// usage
				util.message(sender, "&e/arena create <name>");
				util.message(sender, "&e/arena delete <name>");
				util.message(sender, "&e/arena set <arena> spawn1 <displayname>");
				util.message(sender, "&e/arena set <arena> spawn2 <displayname>");
				util.message(sender, "&e/arena set <arena> name <displayname>");
				util.message(sender, "&e/arena set <arena> lore [displaylore..] (new line is &nl&)");
				util.message(sender, "&e/arena set <arena> glow <displayname>");
				util.message(sender, "&e/arena set <arena> kit");
			} else {
				if (args[0].equalsIgnoreCase("create")) {
					if (args.length == 2) {
						String name = args[1];
						main.getArenas().createNewArena(sender, name);
					} else {
						util.error(sender, "Usage: &e/arena create <name>");
					}
				} else if (args[0].equalsIgnoreCase("delete")) {
					if (args.length == 2) {
						String name = args[1];
						main.getArenas().deleteArena(sender, name);
					} else {
						util.error(sender, "Usage: &e/arena delete <name>");
					}
				} else if (args[0].equalsIgnoreCase("set")) {
					if (args.length == 1) {
						util.message(sender, "&e/arena set <arena> spawn1");
						util.message(sender, "&e/arena set <arena> spawn2");
						util.message(sender, "&e/arena set <arena> name [displayname]");
						util.message(sender, "&e/arena set <arena> lore [displaylore..] (new line is %nl%)");
						util.message(sender, "&e/arena set <arena> glow <true/false>");
						util.message(sender, "&e/arena set <arena> kit");
					} else {
						String name = args[1];
						if (main.getArenas().doesArenaExist(name)) {
							Arena arena = main.getArenas().getArena(name);
							if (args.length >= 3) {
								if (args[2].equalsIgnoreCase("spawn1")) {
									if (sender instanceof Player) {
										Player player = (Player) sender;
										Location loc = player.getLocation();
										arena.setSpawn(0, loc);
										util.message(sender,
												"&eSet spawn for Player 1 at your current location for the arena called &a"
														+ name);
									} else {
										util.error(sender, "Only players can run this command.");
									}
								} else if (args[2].equalsIgnoreCase("spawn2")) {
									if (sender instanceof Player) {
										Player player = (Player) sender;
										Location loc = player.getLocation();
										arena.setSpawn(1, loc);
										util.message(sender,
												"&eSet spawn for Player 2 at your current location for the arena called &a"
														+ name);
									} else {
										util.error(sender, "Only players can run this command.");
									}
								} else if (args[2].equalsIgnoreCase("kit")) {
									if (sender instanceof Player) {
										Player player = (Player) sender;
										List<ItemStack> list = new ArrayList<ItemStack>();
										for (ItemStack is : player.getInventory().getContents()) {
											if (is != null) {
												if (is.getType() != Material.AIR) {
													list.add(is);
												}
											}
										}

										if (!list.isEmpty()) {
											arena.setKit(list);
											util.message(sender,
													"&eSet the Arena Kit to your current inventory for the arena called &a"
															+ name);
										} else {
											util.error(sender, "Unable to set kit to empty inventory.");
										}
									} else {
										util.error(sender, "Only players can run this command.");
									}
								} else if (args[2].equalsIgnoreCase("name")) {
									if (args.length >= 4) {
										String displayName = null;
										for (int i = 3; i < args.length; i++) {
											if (displayName == null) {
												displayName = args[i];
											} else {
												displayName = displayName + " " + args[i];
											}
										}
										arena.setDisplayName(displayName);
										util.message(sender, "&eYou've set the arena displayname to " + displayName);
									} else {
										util.message(sender, "&e/arena set <arena> name [displayname]");
									}
								} else if (args[2].equalsIgnoreCase("lore")) {
									if (args.length >= 4) {
										String loreString = null;
										for (int i = 3; i < args.length; i++) {
											if (loreString == null) {
												loreString = args[i];
											} else {
												loreString = loreString + " " + args[i];
											}
										}
										List<String> lore = new ArrayList<String>();
										if (loreString.contains("%nl%")) {
											String[] parts = loreString.split("%nl%");
											for (String str : parts) {
												lore.add(str);
											}
										} else {
											lore.add(loreString);
										}

										arena.setDisplayLore(lore);

										util.message(sender, "&eYou've set the arena lore to " + lore.toString());
									} else {
										util.message(sender,
												"&e/arena set <arena> lore [displaylore..] (new line is %nl%)");
									}
								} else if (args[2].equalsIgnoreCase("glow")) {
									if (args.length == 4) {
										boolean glow = Boolean.parseBoolean(args[3]);
										arena.setDisplayGlow(glow);
										util.message(sender, "&eSet arena display item glow to &7" + glow);
									} else {
										util.message(sender, "&e/arena set <arena> glow");
									}
								}
							} else {
								util.message(sender, "&e/arena set <arena> spawn1");
								util.message(sender, "&e/arena set <arena> spawn2");
								util.message(sender, "&e/arena set <arena> name <displayname>");
								util.message(sender, "&e/arena set <arena> lore [displaylore..] (new line is &nl&)");
								util.message(sender, "&e/arena set <arena> glow");
								util.message(sender, "&e/arena set <arena> kit");
							}
						} else {
							util.error(sender, "Unable to find arena by that name.");
						}
					}
				}
			}
		}

		return false;
	}

}
