package me.mathiaseklund.duelarena;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;

public class Duels {

	Main main;
	Util util;

	HashMap<String, Duel> duels = new HashMap<String, Duel>(); // DuelID, DuelObject

	HashMap<String, String> duelTarget = new HashMap<String, String>();
	HashMap<String, String> duelArena = new HashMap<String, String>();
	HashMap<String, Double> duelBet = new HashMap<String, Double>();
	HashMap<String, List<String>> duelInvites = new HashMap<String, List<String>>();
	List<String> sentInvites = new ArrayList<String>();

	List<String> selectingTarget = new ArrayList<String>();
	List<String> selectingArena = new ArrayList<String>();
	List<String> settingBet = new ArrayList<String>();

	NamespacedKey targetKey;

	public Duels() {
		main = Main.getMain();
		util = main.getUtil();

		load();
	}

	void load() {
		targetKey = new NamespacedKey(main, "target");
	}

	public void startDuel(Player player) {
		if (!isInDuel(player)) {
			if (!hasSentInvite(player)) {
				Inventory inv = getSelectTargetGUI(player);
				if (inv != null) {
					player.openInventory(inv);
					if (!selectingTarget.contains(player.getUniqueId().toString())) {
						selectingTarget.add(player.getUniqueId().toString());
					}
				} else {
					util.error(player, "Unable to open the Target Selection GUI");
				}
			} else {
				util.error(player, "You already have an outgoing duel request waiting for response.");
			}
		} else {
			util.error(player, "You are already participating in a duel. Try again later.");
		}
	}

	public NamespacedKey getTargetKey() {
		return targetKey;
	}

	public Inventory getSelectTargetGUI(Player player) {
		Inventory inv = Bukkit.createInventory(player, 54,
				ChatColor.translateAlternateColorCodes('&', main.getCfg().getTargetSelectTitle()));
		for (Player p : Bukkit.getOnlinePlayers()) {
			if (!p.getName().equalsIgnoreCase(player.getName())) {
				ItemStack is = new ItemStack(Material.PLAYER_HEAD);
				SkullMeta im = (SkullMeta) is.getItemMeta();
				im.setOwningPlayer(p);
				im.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&e" + p.getName()));
				List<String> lore = new ArrayList<String>();
				lore.add(ChatColor.translateAlternateColorCodes('&', "&7Click to challenge this player."));
				im.setLore(lore);

				PersistentDataContainer container = im.getPersistentDataContainer();
				container.set(targetKey, PersistentDataType.STRING, p.getUniqueId().toString());
				is.setItemMeta(im);
				inv.addItem(is);
			}
		}
		return inv;
	}

	public boolean isSelectingTarget(Player player) {
		return selectingTarget.contains(player.getUniqueId().toString());

	}

	public void setSelectingTarget(Player player, boolean b) {
		if (b) {
			if (!selectingTarget.contains(player.getUniqueId().toString())) {
				selectingTarget.add(player.getUniqueId().toString());
			}
		} else {
			selectingTarget.remove(player.getUniqueId().toString());
		}

	}

	public boolean isSelectingArena(Player player) {
		return selectingArena.contains(player.getUniqueId().toString());

	}

	public void setSelectingArena(Player player, boolean b) {
		if (b) {
			if (!selectingArena.contains(player.getUniqueId().toString())) {
				selectingArena.add(player.getUniqueId().toString());
			}
		} else {
			selectingArena.remove(player.getUniqueId().toString());
		}

	}

	public void setSettingBet(Player player, boolean b) {
		if (b) {
			if (!settingBet.contains(player.getUniqueId().toString())) {
				settingBet.add(player.getUniqueId().toString());
			}
		} else {
			settingBet.remove(player.getUniqueId().toString());
		}
	}

	public boolean isSettingBet(Player player) {
		return settingBet.contains(player.getUniqueId().toString());
	}

	public void selectedTarget(Player player, String target) {
		duelTarget.put(player.getUniqueId().toString(), target);
		util.message(player, "&7Duel target selected.");
		selectArena(player);
	}

	public void selectArena(Player player) {
		Inventory inv = main.getArenas().getArenaSelectionGUI(player);
		player.openInventory(inv);
		setSelectingTarget(player, false);
		setSelectingArena(player, true);
	}

	public void selectedArena(Player player, String name) {
		duelArena.put(player.getUniqueId().toString(), name);
		player.closeInventory();
		setSelectingArena(player, false);
		selectBet(player);
	}

	public void selectBet(Player player) {
		player.spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(
				ChatColor.translateAlternateColorCodes('&', "&ePlease type your desired bet amount in the chat.")));
		util.message(player, "&ePlease type your desired bet amount in the chat.");
		setSettingBet(player, true);
	}

	public void cancelDuelProcess(Player player) {
		util.message(player, "You've cancelled the duel creation process.");
		duelTarget.remove(player.getUniqueId().toString());
		duelArena.remove(player.getUniqueId().toString());
		duelBet.remove(player.getUniqueId().toString());

		setSettingBet(player, false);
	}

	public void selectedBet(Player player, double bet) {
		duelBet.put(player.getUniqueId().toString(), bet);
		setSettingBet(player, false);
		util.message(player, "&eDuel Bet has been set to &6$" + util.formatMoney(bet));
		Player target = Bukkit.getPlayer(UUID.fromString(duelTarget.get(player.getUniqueId().toString())));
		if (target != null) {
			// util.message(player, "&eDuel Invite is being sent to " + target.getName());
			sendDuelInvite(player, target);
		} else {
			util.error(player, "Target player has logged out. Cancelling duel process.");
			cancelDuelProcess(player);
		}
	}

	public void sendDuelInvite(Player sender, Player target) {
		if (!isInDuel(target)) {
			if (!hasSentInvite(target)) {
				List<String> invites = duelInvites.get(target.getUniqueId().toString());
				if (invites == null) {
					invites = new ArrayList<String>();
				}
				if (!invites.contains(sender.getUniqueId().toString())) {
					invites.add(sender.getUniqueId().toString());
					duelInvites.put(target.getUniqueId().toString(), invites);
					sentInvites.add(sender.getUniqueId().toString());
					double bet = duelBet.get(sender.getUniqueId().toString());
					String map = main.getArenas().getArena(duelArena.get(sender.getUniqueId().toString()))
							.getDisplayName();
					List<String> list = main.getCfg().getDuelInviteSent();
					for (String s : list) {
						util.message(sender, s.replace("%target%", target.getName()).replace("%bet%", bet + "")
								.replace("%map%", map));
					}
					list = main.getCfg().getDuelInviteReceived();
					for (String s : list) {
						util.message(target, s.replace("%sender%", sender.getName()).replace("%map%", map)
								.replace("%bet%", bet + ""));
					}

					Bukkit.getScheduler().runTaskLaterAsynchronously(main, new Runnable() {
						public void run() {
							if (hasSentInvite(sender)) {
								sentInvites.remove(sender.getUniqueId().toString());
								List<String> list = duelInvites.get(target.getUniqueId().toString());
								if (list.contains(sender.getUniqueId().toString())) {
									list.remove(sender.getUniqueId().toString());
									duelInvites.put(target.getUniqueId().toString(), list);
									sentInvites.remove(sender.getUniqueId().toString());
									util.error(sender, "Your Duel Invitation has timed out. Try again.");
									util.error(target,
											"The duel request from &e" + sender.getName() + "&7 has timed out.");
								} else {
									util.debug(sender.getName() + " has not invited " + target.getName());
								}
							} else {
								util.debug(sender.getName() + " has not sent any invites.");
							}
						}
					}, 10 * 20);
				} else {
					util.error(sender, "You have already invited " + target.getName() + " to a duel.");
					cancelDuelProcess(sender);
				}
			} else {
				util.error(sender,
						target.getName() + " has an outgoing duel invite request awaiting response. Try again later.");
				cancelDuelProcess(sender);
			}
		} else {
			util.error(sender, target.getName() + " is already partaking in another duel. Try again later.");
			cancelDuelProcess(sender);
		}
	}

	public boolean hasSentInvite(Player player) {
		return sentInvites.contains(player.getUniqueId().toString());
	}

	public void acceptedInvite(Player target, Player sender) {
		List<String> invited = duelInvites.get(target.getUniqueId().toString());
		if (util.isInventoryEmpty(target)) {
			if (invited != null) {
				if (invited.contains(sender.getUniqueId().toString())) {
					double bet = duelBet.get(sender.getUniqueId().toString());
					if (main.getEconomy().getBalance(sender) >= bet) {
						if (main.getEconomy().getBalance(target) >= bet) {
							if (util.isInventoryEmpty(sender)) {
								invited.remove(sender.getUniqueId().toString());
								duelInvites.put(target.getUniqueId().toString(), invited);
								// main.getEconomy().withdrawPlayer(sender, bet);
								// main.getEconomy().withdrawPlayer(target, bet);
								sentInvites.remove(sender.getUniqueId().toString());
								
								Duel duel = new Duel(sender, target, bet,
										duelArena.get(sender.getUniqueId().toString()));
								duels.put(target.getUniqueId().toString(), duel);
								duels.put(sender.getUniqueId().toString(), duel);
							} else {
								util.error(target, sender.getName() + "'s inventory is not empty.");
								util.error(sender,
										"Your inventory is not empty. Please empty your inventory to start a duel.");
							}
						} else {
							util.error(target, "You do not have enough money to partake in the duel.");
						}
					} else {
						util.error(target, sender.getName() + " does not have enough money to partake in the duel.");
					}
				} else {
					util.error(target, sender.getName() + " has not invited you to a duel.");
				}
			} else {
				util.error(target, sender.getName() + " has not invited you to a duel.");
			}
		} else {
			util.error(target, "Your inventory is not empty. Please empty your inventory before starting a duel.");
		}
	}

	public boolean isInDuel(Player player) {
		return duels.containsKey(player.getUniqueId().toString());
	}

	public Duel getPlayerDuel(Player player) {
		return duels.get(player.getUniqueId().toString());
	}

	public void removeInDuel(Player player) {
		duels.remove(player.getUniqueId().toString());
	}

	public void cancelAllDuels(boolean disabled) {
		HashMap<String, Duel> duels = this.duels;
		for (Duel duel : duels.values()) {
			if (!duel.isCancelled()) {
				duel.cancelDuel(disabled);
			}
		}
	}

	public void clearData(Player player) {
		String uuid = player.getUniqueId().toString();
		settingBet.remove(uuid);
		selectingArena.remove(uuid);
		selectingTarget.remove(uuid);
		sentInvites.remove(uuid);
		duelInvites.remove(uuid);
		duelBet.remove(uuid);
		duelArena.remove(uuid);
		duelTarget.remove(uuid);
	}
}
