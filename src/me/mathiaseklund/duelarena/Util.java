package me.mathiaseklund.duelarena;

import java.text.NumberFormat;
import java.util.Locale;
import java.util.regex.Pattern;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import net.md_5.bungee.api.ChatColor;

public class Util {

	Main main;

	public Util() {
		main = Main.getMain();
	}

	/**
	 * Send a debug message to console.
	 * 
	 * @param message
	 */
	public void debug(String message) {
		if (main.getCfg().getDebug()) {
			if (message != null) {
				message(Bukkit.getConsoleSender(), "&7[&eDEBUG&7]&r " + message);
			} else {
				error("Can't debug an empty message.");
			}
		}
	}

	/**
	 * Send a error message to console.
	 * 
	 * @param message
	 */
	public void error(String message) {
		String prefix = "&4ERROR:&7 ";
		message(Bukkit.getConsoleSender(), prefix + message);
	}

	/**
	 * Send a error message to a command sender.
	 * 
	 * @param sender
	 * @param message
	 */
	public void error(CommandSender sender, String message) {
		if (sender != null) {
			if (message != null) {
				String prefix = "&4ERROR:&7 ";
				message(sender, prefix + message);
			} else {
				error(sender, "Unable to report empty error message.");
			}
		} else {
			error("Unable to send error message to NULL receiver");
		}
	}

	/**
	 * Send a message to either the console or a player.
	 * 
	 * @param sender
	 * @param message
	 */
	public void message(CommandSender sender, String message) {
		if (sender != null) {
			if (message != null) {
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
			} else {
				error("Unable to send empty message to " + sender.getName());
			}
		} else {
			error("Unable to send message to NULL receiver.");
		}
	}

	/**
	 * Check if a string is a double value.
	 * 
	 * @param str
	 * @return
	 */
	public boolean isDouble(String str) {
		final Pattern DOUBLE_PATTERN = Pattern
				.compile("[\\x00-\\x20]*[+-]?(NaN|Infinity|((((\\p{Digit}+)(\\.)?((\\p{Digit}+)?)"
						+ "([eE][+-]?(\\p{Digit}+))?)|(\\.((\\p{Digit}+))([eE][+-]?(\\p{Digit}+))?)|"
						+ "(((0[xX](\\p{XDigit}+)(\\.)?)|(0[xX](\\p{XDigit}+)?(\\.)(\\p{XDigit}+)))"
						+ "[pP][+-]?(\\p{Digit}+)))[fFdD]?))[\\x00-\\x20]*");
		return DOUBLE_PATTERN.matcher(str).matches();
	}

	public String formatMoney(double value) {
		String output = NumberFormat.getNumberInstance(Locale.US).format(value);
		return output;
	}

	public boolean isInventoryEmpty(Player player) {
		boolean b = true;
		Inventory inv = player.getInventory();
		for (ItemStack is : inv.getContents()) {
			if (is != null) {
				if (is.getType() != Material.AIR) {
					b = false;
					break;
				}
			}
		}
		return b;
	}
}
