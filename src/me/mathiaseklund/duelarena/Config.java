package me.mathiaseklund.duelarena;

import java.io.File;
import java.util.List;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class Config {

	File f;
	FileConfiguration fc;
	Main main;

	boolean debug;

	String arenaSelectTitle;
	int arenaSelectSize;
	String targetSelectTitle;
	int duelStartTime;

	List<String> duelInviteSent, duelInviteReceived, duelLost, duelWon;

	public Config(File f) {
		main = Main.getMain();
		this.f = f;
		load();
	}

	void load() {
		fc = YamlConfiguration.loadConfiguration(f);

		/**
		 * BOOLEANS
		 */

		debug = fc.getBoolean("debug", true);

		/**
		 * STRINGS
		 */

		arenaSelectTitle = fc.getString("gui.arena-select.title", "&8Select Arena");
		targetSelectTitle = fc.getString("gui.target-select.title", "&8Select Target");

		/**
		 * NUMBERS
		 */

		arenaSelectSize = fc.getInt("gui.arena-select.size", 54);
		duelStartTime = fc.getInt("duel-start-time", 3);
		/**
		 * LISTS
		 */

		duelInviteSent = fc.getStringList("message.duel-invite-sent");
		duelInviteReceived = fc.getStringList("message.duel-invite-received");
		duelLost = fc.getStringList("message.duel-lost");
		duelWon = fc.getStringList("message.duel-won");

		System.out.println("Loaded Config Object");
	}

	/////////////
	// GETTERS //
	/////////////

	public boolean getDebug() {
		return debug;
	}

	public String getTargetSelectTitle() {
		return targetSelectTitle;
	}

	public String getArenaSelectTitle() {
		return arenaSelectTitle;
	}

	public int getArenaSelectSize() {
		return arenaSelectSize;
	}

	public int getDuelStartTime() {
		return duelStartTime;
	}

	public List<String> getDuelInviteSent() {
		return duelInviteSent;
	}

	public List<String> getDuelInviteReceived() {
		return duelInviteReceived;
	}

	public List<String> getDuelLost() {
		return duelLost;
	}

	public List<String> getDuelWon() {
		return duelWon;
	}

	///////////
	// VOIDS //
	///////////

	public void setDebug(boolean b) {
		debug = b;
		fc.set("debug", b);
	}
}
