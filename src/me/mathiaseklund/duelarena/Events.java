package me.mathiaseklund.duelarena;

import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;

public class Events implements Listener {

	Main main = Main.getMain();
	Util util = main.getUtil();

	@EventHandler
	public void onInventoryClose(InventoryCloseEvent event) {
		if (main.getDuels().isSelectingTarget((Player) event.getPlayer())) {
			main.getDuels().setSelectingTarget((Player) event.getPlayer(), false);
		} else if (main.getDuels().isSelectingArena((Player) event.getPlayer())) {
			main.getDuels().setSelectingArena((Player) event.getPlayer(), false);
		}
	}

	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		if (!event.isCancelled()) {
			if (main.getDuels().isSelectingArena((Player) event.getWhoClicked())) {
				event.setCancelled(true);

				ItemStack is = event.getCurrentItem();
				if (is != null) {
					if (is.hasItemMeta()) {
						PersistentDataContainer container = is.getItemMeta().getPersistentDataContainer();
						if (container.has(main.getArenas().getDisplayKey(), PersistentDataType.STRING)) {
							String arena = container.get(main.getArenas().getDisplayKey(), PersistentDataType.STRING);
							main.getDuels().selectedArena((Player) event.getWhoClicked(), arena);
						}
					}
				}
			} else if (main.getDuels().isSelectingTarget((Player) event.getWhoClicked())) {
				event.setCancelled(true);

				ItemStack is = event.getCurrentItem();
				if (is != null) {
					if (is.hasItemMeta()) {
						PersistentDataContainer container = is.getItemMeta().getPersistentDataContainer();
						if (container.has(main.getDuels().getTargetKey(), PersistentDataType.STRING)) {
							String target = container.get(main.getDuels().getTargetKey(), PersistentDataType.STRING);
							main.getDuels().selectedTarget((Player) event.getWhoClicked(), target);
						}
					}
				}
			}
		}
	}

	@EventHandler
	public void onPlayerChat(AsyncPlayerChatEvent event) {
		if (main.getDuels().isSettingBet(event.getPlayer())) {
			String msg = event.getMessage();
			event.setCancelled(true);

			if (util.isDouble(msg)) {
				double bet = Double.parseDouble(msg);
				if (bet > 0) {
					if (main.getEconomy().getBalance(event.getPlayer()) >= bet) {
						main.getDuels().selectedBet(event.getPlayer(), bet);
					} else {
						util.error(event.getPlayer(), "You do not have enough balance to place this bet. Try again.");
					}

				} else {
					util.error(event.getPlayer(), "Bet amount must be above 0. Type 'cancel' to cancel duel process.");
				}
			} else {
				if (msg.equalsIgnoreCase("cancel")) {
					main.getDuels().cancelDuelProcess(event.getPlayer());
				} else {
					util.error(event.getPlayer(),
							"Bet amount has to be a monetary value. Type 'cancel' to cancel duel process.");
				}
			}
		}
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
		if (event.getDamager() instanceof Player && event.getEntity() instanceof Player) {
			Player dmger = (Player) event.getDamager();
			Player dfndr = (Player) event.getEntity();
			if (main.getDuels().isInDuel(dmger)) {
				Duel duel = main.getDuels().getPlayerDuel(dmger);
				if (duel.isStarted()) {
					if (duel.getTarget().getName().equalsIgnoreCase(dfndr.getName())
							|| duel.getSender().getName().equalsIgnoreCase(dfndr.getName())) {
						// do nothing.

						double finalDamage = event.getFinalDamage();
						double health = dfndr.getHealth();
						if (health - finalDamage <= 0) {
							event.setCancelled(true);
							duel.killed(dfndr);
						}
					} else {
						event.setCancelled(true);
					}
				} else {
					event.setCancelled(true);
				}
			}
		} else if (event.getDamager() instanceof Projectile && event.getEntity() instanceof Player) {
			Projectile proj = (Projectile) event.getDamager();
			if (proj.getShooter() instanceof Player) {
				Player dmger = (Player) proj.getShooter();
				Player dfndr = (Player) event.getEntity();
				if (main.getDuels().isInDuel(dmger)) {
					Duel duel = main.getDuels().getPlayerDuel(dmger);
					if (duel.isStarted()) {
						if (duel.getTarget().getName().equalsIgnoreCase(dfndr.getName())
								|| duel.getSender().getName().equalsIgnoreCase(dfndr.getName())) {

						} else {
							event.setCancelled(true);
						}
					} else {
						event.setCancelled(true);
					}
				}
			}
		}
	}

	@EventHandler
	public void onDisconnect(PlayerQuitEvent event) {
		if (main.getDuels().isInDuel(event.getPlayer())) {
			main.getDuels().getPlayerDuel(event.getPlayer()).cancelDuel(false);
		}

		main.getDuels().clearData(event.getPlayer());
	}

}
